# Pawn Tactics Freedom

An open source Pawn Tactics client started from scratch using Haxe and OpenFl.    
  
This project was created in 2019 with the intention of replacing the Action Script 3 Pawn Tactics client and obtaining 
freedom from its legacy code and Flash Player. 

## Building Instructions

- [Install OpenFl](https://www.openfl.org/download/) using haxelib
- [Setup your code editor](https://www.openfl.org/learn/haxelib/docs/getting-started/choose-a-code-editor/)
- Run `openfl test html5`

This project is licensed using MPL 2.0.    

Files are Copyright (c) 2022 Alex (Pawn Studios), and Future Contributors.