/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package handlers;
import ui.Views;
class ConnectedHandler {
    public function new() {}

    public function handle():Void {
        Views.landingScreen.getServerStatusText().text = "Connected.";
    }
}
