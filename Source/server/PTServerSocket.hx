/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package server;
import handlers.ConnectedHandler;
import openfl.utils.Timer;
import openfl.utils.ByteArray;
import openfl.errors.Error;
import openfl.utils.Function;
import openfl.events.*;
import openfl.net.Socket;
class PTServerSocket {
    private var socket:Socket = new Socket();
    private var gameServerIP:String = "localhost";
    private var gameServerPort:Int = 9340;

    private var connectsRemaining:Int = 12;
    private var communicationTimeout:Timer = new Timer(10000, 1);
    private var timer:Timer = new Timer(1000, 1);

    private var remoteData:ByteArray = new ByteArray();
    private var len:Int = -1;

    private var connectedHandler:ConnectedHandler = new ConnectedHandler();

    public var onConnect:(Void) -> Void = null;
    public var onError:(Void) -> Void = null;

    public function new() {
        socket.addEventListener(Event.CONNECT, onConnection);
        socket.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
        socket.addEventListener(ProgressEvent.SOCKET_DATA, onDx);
        socket.addEventListener(Event.CLOSE, onClose);
        socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onIoError);
        socket.timeout = 2000;

        timer.addEventListener(TimerEvent.TIMER, connect);
    }

    public function requestLoginInfo():Void {
        var ba:ByteArray = new ByteArray();
        ba.writeShort(1);
        ba.writeByte(127);
        ba.position = 0;
        socket.writeBytes(ba);
        socket.flush();
        communicationTimeout.start();
    }

    private function timeout(e:Event):Void {
        if (socket.connected) {
            socket.close();
        }
        if (onError != null) {
            onError();
        }
    }

    private function onIoError(e:Event):Void {
        if (connectsRemaining > 0) {
            connectsRemaining--;
            if (connectsRemaining == 0) {
                socket.timeout = 4500;
            }
            if (Std.is(e, SecurityErrorEvent)) {
                connect();
            }
            else {
                timer.start();
            }
        }
        else {
//            displayError("Server Down", "Failed to locate the Pawn Tactics servers. Please check the forum for updates.");
        }
    }

    public function connect(e:Event = null):Void {
        socket.connect(gameServerIP, gameServerPort);
        communicationTimeout.addEventListener(TimerEvent.TIMER, timeout);
    }

    private function onClose(e:Event):Void {
        communicationTimeout.stop();
//        displayError("Communications Error", "The server has closed the connection.");
    }

    private function onConnection(e:Event):Void {
        timer.stop();

        connectedHandler.handle();

        if (onConnect != null) {
            onConnect();
        }
    }

    private function onDx(e:Event = null):Void {
        communicationTimeout.stop();
        try {
            if (len == -1) {
                if (socket.bytesAvailable >= 2) {
                    len = socket.readShort();
                }
                else {
                    return;
                }
            }

            while (socket.bytesAvailable > 0 && len > 0) {
                remoteData.writeByte(socket.readByte());
                len--;
            }

            if (len == 0) {
                remoteData.position = 0;
                onData(remoteData);
                len = -1;
                remoteData.position = 0;
                remoteData.length = 0;

                onDx();
            }
        } catch (e) {
//            displayError("Communications Error", "An error has occurred communicating with the server.");
            throw e;
        }
    }

    private function onData (data:ByteArray):Void {
        var type:Int = data.readByte();

        switch(type) {
            case 127:
        }
    }

    public function destroy():Void {
        socket.removeEventListener(Event.CONNECT, onConnection);
        socket.removeEventListener(IOErrorEvent.IO_ERROR, onIoError);
        socket.removeEventListener(ProgressEvent.SOCKET_DATA, onDx);
        socket.removeEventListener(Event.CLOSE, onClose);
        socket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onIoError);
        socket.close();

        remoteData.clear();

        communicationTimeout.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
        communicationTimeout.stop();
    }
}
