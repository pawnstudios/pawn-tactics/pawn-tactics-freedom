/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package presentations;
import flash.display.DisplayObjectContainer;
import openfl.display.DisplayObject;
class Presentation implements IPresentation {
    private var parent:DisplayObjectContainer;
    private var displayObject:DisplayObject;


    public function new(parent:DisplayObjectContainer, displayObject:DisplayObject) {
        this.parent = parent;
    }

    public function render() {
        root.addChild(mc)
    }
}
