/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package presentations;
import openfl.utils.Function;
import ui.Views;
import ui.Windows;
class LoginRegisterPresentation {
    public var onAccountEnter:Function = null;

    public function new () {
    }

    public function go() {
        Windows.openWindow(Windows.loginWindow);
        Windows.setWindowPosition(Windows.loginWindow, 496.0, 350.0);
        Windows.loginWindow.onAccountEnter = _onAccountEnter;
    }

    private function _onAccountEnter(account:String, password:String):Void {
        if (onAccountEnter != null) {
            onAccountEnter(account, password);
        }
    }
}
