/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package managers;

class PresentationManager {
    private var parent:DisplayObjectContainer;

    private var loginPresentation:LoginPresentation;
    
    public function new(parent:DisplayObjectContainer) {
        this.parent = parent;
        loginPresentation = new LoginPresentation();
    }
    
    public function initialize():Void {
        parent.addChild(loginPresentation.toDisplayObject();
    }
}