/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package assets;
import openfl.errors.Error;
import openfl.events.*;
import openfl.utils.Function;
import openfl.display.Loader;
import flash.net.URLRequest;

class AssetLoader {
    private var assetLocation:URLRequest;
    private var loader:Loader = new Loader();

    public var onProgress:Function = null;
    public var onCompletion:Function = null;
    public var onError:Function = null;

    public function new(location:URLRequest) {
        this.assetLocation = location;

        //loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onGameLoadProgress);
        loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onGameLoadCompletion);
       // loader.contentLoaderInfo.addEventListener(Event.OPEN, onGameLoadStart);
        loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIoTxError);
    }

    public function load():Void {
        loader.load(assetLocation);
    }

    private function onGameLoadCompletion(event:Event):Void {
     //   loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onGameLoadProgress);
        loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onGameLoadCompletion);
       // loader.contentLoaderInfo.removeEventListener(Event.OPEN, onGameLoadStart);
        loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIoTxError);

        if (onCompletion != null) {
            onCompletion();
        }

    }

    private function onIoTxError(event:IOErrorEvent):Void {
        if (onError != null) {
            onError();
        }
    }

}
