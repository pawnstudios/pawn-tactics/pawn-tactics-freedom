/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ui;

import flash.ui.Keyboard;
import openfl.events.KeyboardEvent;
import openfl.utils.Function;
import flash.text.TextField;

class LoginWindow extends swf.ui.LoginWindow {
    public var onAccountEnter:Function = null;

    public function new() {
        super();

        getAccountInput().addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        getPasswordInput().addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    }

    public function getAccountInput():TextField {
        return this.accountInput;
    }

    public function getPasswordInput():TextField {
        return this.passwordInput;
    }

    private function onKeyDown(e:KeyboardEvent):Void {
        if (e.keyCode == Keyboard.ENTER) {
            if (onAccountEnter != null) {
                onAccountEnter(getAccountInput().text, getPasswordInput().text);
            }
        }
    }
}

