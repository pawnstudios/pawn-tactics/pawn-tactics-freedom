/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ui;
import flash.display.DisplayObjectContainer;
import openfl.display.Stage;
import flash.display.MovieClip;
class Views {
    private static var displayList:DisplayObjectContainer;

    public static var landingScreen(default, null):LandingScreen;

    public static function switchTo(view:MovieClip):Void {
        displayList.addChild(view);
    }

    public function new (root:DisplayObjectContainer):Void {
        displayList = root;
        landingScreen = new LandingScreen();
    }
}
