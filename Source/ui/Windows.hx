/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ui;
import openfl.display.DisplayObjectContainer;
import openfl.display.Stage;
import openfl.display.MovieClip;

class Windows {
    private static var displayList:DisplayObjectContainer;

    public static var loginWindow(default, null):LoginWindow;

    public static function openWindow(window:MovieClip):Void {
        displayList.addChild(window);
    }

    public static function closeWindow(window:MovieClip):Void {
        displayList.removeChild(window);
    }

    public static function setWindowPosition(window:MovieClip, x:Float, y:Float):Void {
        window.x = x;
        window.y = y;
    }

    public function new (root:DisplayObjectContainer):Void {
        displayList = root;
        loginWindow = new LoginWindow();
    }
}
