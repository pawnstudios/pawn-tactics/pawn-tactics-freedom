/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ui;
import openfl.text.TextField;
import swf.ui.LandingScreen;

class LandingScreen extends swf.ui.LandingScreen {
   // public static var instance:swf.ui.LandingScreen = new LandingScreen();

  //  public static var _versionText:TextField = instance.versionText;
   // public static var _serverStatusText:TextField = instance.serverStatusText;


    public function new () {
        super();
    }

    public function getVersionText():TextField {
        return this.versionText;
    }

    public function getServerStatusText():TextField {
        return this.serverStatusText;
    }
}