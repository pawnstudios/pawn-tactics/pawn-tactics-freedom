/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ui;
import openfl.events.MouseEvent;
import openfl.utils.Function;
import swf.ui.RegisterButton;

class RegisterButton extends swf.ui.RegisterButton {
    public var onClick:Function = null;

    public function new() {
        super();
        this.addEventListener(MouseEvent.CLICK, _onClick);
    }

    private function _onClick(e:MouseEvent):Void {
        if (onClick != null) {
            onClick();
        }
    }
}
