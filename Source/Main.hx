/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package;

import presentations.LoginRegisterPresentation;
import server.PTServerSocket;
import assets.AssetLoader;
import ui.*;
import openfl.display.Sprite;

class Main extends Sprite {
    private var assetLoader:AssetLoader;
    private var server:PTServerSocket = new PTServerSocket();
    private var loginPresentation:LoginRegisterPresentation = new LoginRegisterPresentation();

    public function new () {
        super();

        new Windows(this);
        new Views(this);

        server.connect();
        server.onConnect = function () {
            login.open();
        };

    }
}