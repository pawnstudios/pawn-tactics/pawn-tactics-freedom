/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package environment;
class EnvironmentDetect {
    public var isBrowser:Bool = false;
    public var isDesktop:Bool = false;

    public var isPasswordLogin:Bool = false;

    public var isSocialMediaLogin:Bool = false;

    public var isDiscordLogin:Bool = false;
    public var isXenForoLogin:Bool = false;
    public var isFacebookLogin:Bool = false;
    public var isTwitterLogin:Bool = false;
    public var isTwitchLogin:Bool = false;
    public var isInstagramLogin:Bool = false;


    public function new() {
    }
}
