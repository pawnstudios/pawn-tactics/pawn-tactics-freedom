/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ;

import presentations.LoginRegisterPresentation;
import presentations.login.ConnectingPresentation;

class Login {
    private var connectedPresentation:ConnectingPresentation = new ConnectingPresentation();
    private var loginPresentation:LoginRegisterPresentation = new LoginRegisterPresentation();

    public function new() {}

    public function open() {

    }

    public function onAccountEnter(account:String, password:String) {

    }
}
