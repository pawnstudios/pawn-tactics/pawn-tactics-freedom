/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package requests;
import openfl.utils.ByteArray;
interface Request {
    public function getType():Int;
    public function getOperation():Int;
    public function getData():ByteArray;
}
