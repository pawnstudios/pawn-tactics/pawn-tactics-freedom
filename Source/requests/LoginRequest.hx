/*
Copyright (c) 2022  Alex (Pawn Studios)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package requests;
class LoginRequest implements Request {
    private var type:Int;
    private var operation:Int;

    private function new() {}

    public static function newLoginPasswordRequest(account:String, password:String):LoginRequest {

    }

    public function getType():Int {

    }

    public function getOperation():Int {

    }
}
